# Copyright 2021-2023 Timo Gurr <tgurr@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require github [ user=fwupd release=${PV} suffix=tar.xz ] \
    python [ blacklist=2 multibuild=false ] \
    meson

SUMMARY="EFI executable for fwupd"
HOMEPAGE+=" https://www.fwupd.org"

LICENCES="LGPL-2.1"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS=""

DEPENDENCIES="
    build:
        dev-python/pefile[python_abis:*(-)?]
        sys-boot/gnu-efi
        sys-devel/binutils
"

MESON_SRC_CONFIGURE_PARAMS=(
    -Defi-includedir=/usr/$(exhost --target)/include/efi
    -Defi-libdir=/usr/$(exhost --target)/lib
    -Defi_sbat_distro_id="exherbo"
)

src_prepare() {
    meson_src_prepare

    edo sed \
        -e "s:\[ld:\['${LD}':g" \
        -i efi/meson.build
}

