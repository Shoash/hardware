# Copyright 2008, 2010 Mike Kelly
# Distributed under the terms of the GNU General Public License v2

require github [ user=fenrus75 tag="v${PV}" ] autotools [ supported_autoconf=[ 2.5 ] supported_automake=[ 1.12 ] ]
require bash-completion systemd-service [ systemd_files=[ "${PN}".service ] ]

SUMMARY="A tool for reducing power usage"
DESCRIPTION="
Computer programs can make your computer use more power. PowerTOP is a
Linux tool that helps you find those programs that are misbehaving while
your computer is idle.
"
HOMEPAGE="https://01.org/powertop"

LICENCES="GPL-2"
SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="
    ( linguas: ca cs_CZ de_DE en_GB en_US es_ES hu_HU id_ID nl_NL zh_TW )
"

DEPENDENCIES="
    build:
        sys-devel/gettext[>=0.18]
        virtual/pkg-config
    build+run:
        net-libs/libnl:=
        sys-apps/pciutils
        sys-libs/ncurses
        sys-libs/zlib
    suggestion:
        net-wireless/bluez [[ description = [ For disabling inactive bluetooth connections ] ]]
        net-wireless/wireless_tools [[ description = [ For enabling power-saving wireless settings ] ]]
        x11-apps/xrandr [[ description = [ For disabling TV output ] ]]
"

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --enable-nls
    --disable-static
)

src_prepare() {
    default
    # eautoreconf fails with "configure.ac:28: error: required file './config.rpath' not found"
    autotools_select_versions
    edo ./autogen.sh
}

src_install() {
    default

    if option !bash-completion; then
        # Bash completion is unconditionally installed
        edo rm -r "${IMAGE}"/usr/share/bash-completion/
    fi

    keepdir /var/cache/powertop
    install_systemd_files
}

