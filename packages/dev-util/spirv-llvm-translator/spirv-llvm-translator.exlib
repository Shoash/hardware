# Copyright 2019-2022 Timo Gurr <tgurr@exherbo.org>
# Copyrgiht 2023 Bernd Steinhauser <berniyh@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require github [ user=KhronosGroup pn=SPIRV-LLVM-Translator tag=v${PV} ]
require cmake
require alternatives

SUMMARY="LLVM/SPIR-V Bi-Directional Translator"
DESCRIPTION="
Library and tool for translation between LLVM IR and SPIR-V.
"

LICENCES="UoI-NCSA"
SLOT="$(ever major)"
MYOPTIONS=""

DEPENDENCIES="
    build:
        virtual/pkg-config
    build+run:
        dev-lang/llvm:${SLOT}
        dev-lang/spirv-tools
        sys-libs/spirv-headers[>=1.5.5]
"

LLVM_PREFIX="/usr/$(exhost --target)/lib/llvm/${SLOT}"

CMAKE_SRC_CONFIGURE_PARAMS=(
    -DBUILD_SHARED_LIBS:BOOL=TRUE
    -DCMAKE_BUILD_TYPE:STRING=Release
    -DCMAKE_POSITION_INDEPENDENT_CODE:BOOL=TRUE
    -DCMAKE_INSTALL_PREFIX:STRING="${LLVM_PREFIX}"
    -DLLVM_DIR:PATH="${LLVM_PREFIX}"/lib/cmake/llvm
    -DLLVM_BUILD_TOOLS:BOOL=TRUE
    -DLLVM_EXTERNAL_SPIRV_HEADERS_SOURCE_DIR:PATH="/usr/$(exhost --target)/include/spirv"
    -DLLVM_SPIRV_BUILD_EXTERNAL:BOOL=TRUE
)

src_install() {
    cmake_src_install

    # Fallout of 9e8725bf857bcc49d6ebf27b0afe4d521de719c8 when building as shared libs.
    # /usr/x86_64-pc-linux-gnu/lib/llvm/13/bin/llvm-spirv: error while loading shared libraries:
    # libLLVMSPIRVLib.so.13: cannot open shared object file: No such file or directory
    dosym ${LLVM_PREFIX}/lib/libLLVMSPIRVLib.so.${SLOT} /usr/$(exhost --target)/lib/libLLVMSPIRVLib.so.${SLOT}

    # we need the pc file in the normal pkgconfig folder, otherwise pkgconfig won't find it.
    arch_dependent_alternatives=(
        /usr/$(exhost --target)/lib/pkgconfig/LLVMSPIRVLib.pc /usr/$(exhost --target)/lib/llvm/${SLOT}/lib/pkgconfig/LLVMSPIRVLib.pc
        /usr/$(exhost --target)/bin/llvm-spirv /usr/$(exhost --target)/lib/llvm/${SLOT}/bin/llvm-spirv
    )

    alternatives_for _$(exhost --target)_${PN} ${SLOT} ${SLOT} "${arch_dependent_alternatives[@]}"
}

